# Nodejs Angularjs Training

Objective: Learn a technology stack while developing a new app.

##Development process
- Fork the repository in bitbucket.
- Clone your for to your local environment:
```
#!console
git clone git@bitbucket.org:<user>/training.git
```

- Add the central repository as upstream:
```
#!console
git remote add upstream git@bitbucket.org:nodeangulartraining/training.git
```

- When developing, get the latest code from upstream and push your code to origin:
```
#!console
git fetch upstream
git rebase upstream/master
git push origin <branch>
```

- Open a pull request in bitbucket.

##Setup

- Install compass
```
#!console
gem install compass
```

- Install mongoDB

In the app directory, run:

```
#!console
npm install // Required to install grunt.
grunt setup // Install all dependencies.
```

## Run and Debug
```
#!console

grunt
```

## Node modules installed so far

### Prod
- restify: REST framework.
- mongoose: MongoDB ODM.
- jsonwebtoken: Handle authentication with tokens.
- winston: Logger. More info in: https://github.com/flatiron/winston/
- socket.io: websockets server/client. More info in: https://github.com/Automattic/socket.io

### Dev
- karma: Spectacular Test Runner for JavaScript.
- mocha: simple, flexible, fun test framework.
- chai: BDD/TDD assertion library for node.js and the browser. Test framework agnostic.
- karma-mocha: A Karma plugin. Adapter for Mocha testing framework.
- karma-chai: Chai for Karma. Assertion library.
- karma-sinon: Sinon for Karma. Mocks library.
- karma-phantomjs-launcher: A Karma plugin. Launcher for PhantomJS.

### Grunt Modules

- grunt-concurrent: Runs slow tasks concurrently, potentially improving the build time significantly. More info: https://github.com/sindresorhus/grunt-concurrent
- grunt-contrib-clean: Clean files and folders. More info: https://github.com/gruntjs/grunt-contrib-clean
- grunt-contrib-requirejs: Optimize RequireJS projects using r.js. More info: https://github.com/gruntjs/grunt-contrib-requirejs
- grunt-contrib-watch: Run predefined tasks whenever watched file patterns are added, changed or deleted. More info: https://github.com/gruntjs/grunt-contrib-watch
- grunt-contrib-compass: Compile Sass to CSS using Compass. More info: https://github.com/gruntjs/grunt-contrib-compass
- grunt-contrib-jshint: Validate files with JSHint. More info: https://github.com/gruntjs/grunt-contrib-jshint
- grunt-jshint-stylish: Stylish reporter for jshint. More info: https://github.com/sindresorhus/jshint-stylish
- grunt-node-inspector: Run node-inspector as a grunt task for easy configuration and integration with the rest of your workflow. More info: https://github.com/ChrisWren/grunt-node-inspector
- grunt-nodemon: Run nodemon as a grunt task for easy configuration and integration with the rest of your workflow. More info: https://github.com/ChrisWren/grunt-nodemon
- grunt-open: Open urls and files from a grunt task. More info: https://github.com/jsoverson/grunt-open
- grunt-karma: Run tests with karma. More info: https://github.com/karma-runner/grunt-karma
- time-grunt: Displays the execution time of grunt tasks. More info: https://github.com/sindresorhus/time-grunt

- grunt-contrib-concat: Concatenate files. More info: https://github.com/gruntjs/grunt-contrib-concat (Not configured yet)
- grunt-contrib-uglify: Minify files with UglifyJS. More info: https://github.com/gruntjs/grunt-contrib-uglify (Not configured yet) 

## The stack

### Must Have
* Node.js
* AngularJS
* Yeoman
* Grunt
* Karma
* Mocha + Chai

### Nice to have
* Jenkis
* Restify
* Redis
* REST
* SASS/CSS
* RequireJS
* JSLint
