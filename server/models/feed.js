'use strict';

// set up mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedSchema = new Schema({
  type: {type: String},
  uri: {type: String},
  remoteId: {type: String}
});

var Feed = mongoose.model('Feed', FeedSchema);

exports.feedModel = Feed;
