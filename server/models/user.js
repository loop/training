'use strict';

// set up mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  name: {type: String},
  password: {type: String},
  feeds: [{type: Schema.Types.ObjectId, ref: 'Feed'}]
});

var User = mongoose.model('User', UserSchema);

exports.userModel = User;
