'use strict';

// set up mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NewsSchema = new Schema({
  title: {type: String},
  remoteId: {type: String, unique: true},
  date: {type: Date},
  deleted: {type: Boolean},
  body: {type: String},
  feeds: [{type: Schema.Types.ObjectId, ref: 'Feed'}]
});

var News = mongoose.model('News', NewsSchema);
exports.newsModel = News;
