var User = require('./user');
var Feed = require('./feed');
var News = require('./news');

exports.userModel = User.userModel;
exports.feedModel = Feed.feedModel;
exports.newsModel = News.newsModel;
