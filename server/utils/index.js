/**
 * A collection of util functions.
 */

'use strict';

module.exports.parseCookie = function(str, opt) {
  opt = opt || {};
  var obj = {};
  var pairs = str.split(/[;,] */);
  var dec = opt.decode || decodeURIComponent;

  pairs.forEach(function(pair) {
    var eqIdx = pair.indexOf('=');

    // skip things that don't look like key=value
    if (eqIdx < 0) {
      return;
    }

    var key = pair.substr(0, eqIdx).trim();
    var val = pair.substr(++eqIdx, pair.length).trim();

    // quoted values
    if ('"' === val[0]) {
      val = val.slice(1, -1);
    }

    // only assign once
    if (undefined === obj[key]) {
      try {
        obj[key] = dec(val);
      } catch (e) {
        obj[key] = val;
      }
    }
  });

  return obj;
};
