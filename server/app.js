/**
 * Main application file
 */
'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var restify = require('restify');
var mongoose = require('mongoose');
var config = require('./config');
var logger = require('./services/logger');

mongoose.connect(config.database.url);
var db = mongoose.connection;
db.on('error', function (err) {
  logger.error('error opening connection to mongo: ' + err);
});
db.once('open', function () {
  logger.info('Successfully connected to MongoDB');
});

// Setup server
var server = restify.createServer();
server.use(restify.queryParser());
server.use(restify.bodyParser());

require('./routes')(server);

server.get(/\/.*/, restify.serveStatic({
  directory: config.root + '/client',
  default: 'index.html'
}));

// Start server
server.listen(config.port, function () {
  logger.info('%s listening at %s', server.name, server.url);
});

// Start sockets server
var io = require('socket.io')(config.ioPort);
io.on('connection', require('./services/socket').init);

// Expose server
exports = module.exports = server;

// Start twitter stream
require('./services/twitter-stream').start();
