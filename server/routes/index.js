/**
* Main application routes
*/

'use strict';

var News = require('../models').newsModel;
var authentication = require('../services/authentication');
var feedsService = require('../services/feeds');

module.exports = function(server) {

  // Insert routes below
  server.get('/api/news', function (req, res) {
    var query,
      start = req.query.start ? req.query.start : 0;
    authentication.getLoggedUser(req).then(function (user) {
      if (user) {
        var filter = req.query.feedId? {feeds: req.query.feedId} :{};
        query = News.find(filter)
          .where('feeds')
          .in(user.feeds)
          .select('title body date')
          .sort('-date')
          .skip(start)
          .limit(20);
        query.exec().then(function (news) {
          res.json(news);
        });
      }
    }, function () {
      // TODO return default news.
      query = News.find(null, 'title body date', {sort: '-date'}).skip(start).limit(20);
      query.exec().then(function (news) {
        res.json(news);
      });
    });
  });

  /**
   * Feed endpoints.
   */
  server.post('/api/feed', feedsService.createFeed);

  server.get('/api/feeds', feedsService.getFeeds);

  server.del('/api/feed/:feedId', feedsService.removeFeed);

  /**
   * Authentication endpoints.
   */
  server.post('/api/register', authentication.register);

  server.post('/api/login', authentication.login);

  server.get('/api/session', authentication.session);

  server.get('/api/logout', authentication.logout);
};
