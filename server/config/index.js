'use strict';

var path = require('path');

var all = {
  // Root path of server
  root: path.normalize(__dirname + '/../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Socket server port
  ioPort: 9001,

  database: {
    url: 'mongodb://localhost/training'
  },

  // token configuration
  token: {
    // should be a large unguessable string
    secret: '4ceb0cb17a82bbed5321974c849af621',
    options: {
      expiresInMinutes: 60 * 24 * 30 // one month
    }
  },

  /* jshint camelcase:false */
  twitterApiCredentials : {
    consumer_key: '3K35VKO4i4ZyaFvm3nme4jmGI',
    consumer_secret: 'qiVHsdaOeHdqDoKlvkKYINwUXHACGnFBXPv8ZxoqdtESg0R6XZ',
    access_token_key: '2752170384-6b6WSRyBwrzuMtZYuzEs1SDfUqsJ0AkwsRmO1fW',
    access_token_secret: 'CXmFOBqpK04pHWuHOJ3rPpk6XJZzjYj9PzKFJPA5R9H2v'
  },
  //This is needed because we are using 2 streaming connections to public endpoints.
  twitterApiCredentialsHashtags : {
    consumer_key: 'pToKO25IGX256w4jKjVM5Udmz',
    consumer_secret: 'iCMvagN3KNQ7h8bLoQ1gHzzMlgccjYqc56PKFVPi1taRThUXrf',
    access_token_key: '2773626462-ywhHxBfBG4p2LSBUithIdCWGDYNZOGxbnRKqF8v',
    access_token_secret: 'CATGS0vDPXEcirbwMUs4XGdeEJseis2exSdrQdm6fx81A'
  }
  /* jshint camelcase:true */
};

module.exports = all;
