/**
 * A winston logger instance.
 */
'use strict';

var winston = require('winston');

module.exports = new winston.Logger ({
  transports : [
    new (winston.transports.Console)(),
    new (winston.transports.File)({filename: 'server.log'})
  ]
});
