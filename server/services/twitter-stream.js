/**
 * Connect to the twitter api and fetch the news.
 */

'use strict';

var News = require('../models/index').newsModel;
var Feed = require('../models/index').feedModel;
var _ = require('lodash');
var logger = require('./logger');
var twitter = require('./immortal-ntwitter');
var config = require('../config/index');
var socketsService = require('./socket');
var twitterText = require('twitter-text');

var saveNews = function (data, feedIds) {
  var urls = data.entities.urls ? data.entities.urls : [];
  var media = data.entities.media ? data.entities.media : [];
  var entities = urls.concat(media);

  data.text = twitterText.autoLink(data.text, {
    urlEntities: entities,
    targetBlank: true
  });

  var dataToSave = {
    /*jshint camelcase: false */
    title: data.user.screen_name,
    remoteId: data.id_str,
    date: new Date(data.created_at),
    /*jshint camelcase: true */
    $addToSet: { feeds: { $each : feedIds}},
    deleted: false,
    body: data.text
  };

  var query = {
    /*jshint camelcase: false */
    remoteId : data.id_str
    /*jshint camelcase: true */
  };

  News.findOneAndUpdate(query, dataToSave, {upsert : true},
    function (err, savedNews) {
      if (err) {
        logger.error('Error to save news :' + err);
      } else {
        //@todo this should emmit by user.feeds
        //don't emmit twice (on feed updates)
        if (savedNews.feeds.length === 1) {
          socketsService.notifySockets(savedNews);
        }
        logger.info('News from twitter saved.');
      }
    }
  );
};

var destroyStream = function(){};
var startStream = function () {
  var query = Feed.find().or([{type: 'twitter'},{type: 'twitterHashtag'}]);
  query.exec().then(function (feeds) {
    if (feeds.length > 0) {
      var idsFollow = [];
      var hashtagsFollow = [];
      var userFeeds = {};
      var hashtagsFeeds = {};
      var streamUsers = function(){};
      var streamHashtags = function(){};
      var populateIndexes = function (dbFeed) {
        if (dbFeed.type === 'twitter') {
          idsFollow.push(dbFeed.remoteId);
          userFeeds[dbFeed.remoteId] = dbFeed;
        } else {
          hashtagsFollow.push('#' + dbFeed.remoteId);
          hashtagsFeeds[dbFeed.remoteId] = dbFeed;
        }
      };

      _.map(feeds, populateIndexes);

      var twit = new twitter(config.twitterApiCredentials);
      twit.verifyCredentials(function (err) {
        if (idsFollow.length > 0) {
          if (!err) {
            twit.immortalStream(
              'statuses/filter', {follow: idsFollow.join(',')},
              function (stream) {
              logger.info('Twitter users stream started.');
              streamUsers = stream;
              stream.on('data', function (data) {
                /*jshint camelcase: false */
                var currentFeed = userFeeds[data.user.id_str];
                /*jshint camelcase: true */
                if (currentFeed) {
                  saveNews(data, [currentFeed._id]);
                }
              });
            });
          } else {
            logger.error(err);
          }
        }
      });
      var twitHashtags = new twitter(config.twitterApiCredentialsHashtags);
      twitHashtags.verifyCredentials(function (err) {
        if (hashtagsFollow.length > 0) {
          if (!err) {
            twitHashtags.immortalStream(
              'statuses/filter', {track: hashtagsFollow.join(',')},
              function (stream) {
              logger.info('Twitter hashtags stream started.');
              streamHashtags = stream;
              stream.on('data', function (data) {
                var hashtags = data.entities.hashtags;
                var feedIds = [];
                for (var i = 0; i < hashtags.length; i++) {
                  var presentFeed = hashtagsFeeds[hashtags[i].text];
                  if (presentFeed) {
                    feedIds.push(presentFeed._id);
                  }
                }
                saveNews(data, feedIds);
              });
            });
          } else {
            logger.error(err);
          }
        }
      });
      destroyStream = function() {
        streamUsers.destroy();
        streamHashtags.destroy();
      };
    }
  });
};

module.exports = {
  start : startStream,
  restart : function () {
    destroyStream();
    startStream();
  },
  saveNews : saveNews
};
