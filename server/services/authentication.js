/**
 * Authentication setup file
 */
'use strict';

var secret = require('../config/index').token.secret;
var options = require('../config/index').token.options;
var sockets = require('../services/socket');
var jwt = require('jsonwebtoken');
var User = require('../models/index').userModel;
var jQuery = require('jquery-deferred');
var validator = require('validator');

/**
 * Returns the logged user using the token in the authorization header.
 *
 * @param req The request object.
 * @returns A Promise object.
 */
var getLoggedUser = function (req) {
  var deferred = jQuery.Deferred();
  var token = req.headers.authorization ? req.headers.authorization : '';
  if (token) {
    jwt.verify(token, secret, options, function (err, decoded) {
      if (!err && decoded) {
        User.findById(decoded.userId, function (err, user) {
          if (!err && user) {
            deferred.resolve(user);
          } else {
            deferred.reject();
          }
        });
      }
    });
  } else {
    deferred.reject();
  }
  return deferred.promise();
};

/**
 * Register a new user.
 *
 * @param req The request object. Must contain username and password params.
 * @param res The response object.
 */
module.exports.register = function (req, res) {
  try {
    if(!validator.isEmail(req.params.username)) {
      throw 'The username must be a valid email address.';
    }

    if(!validator.isLength(req.params.username, 6) &&
       !validator.isAlphanumeric(req.params.username)) {
      throw 'The password must match, have at least 6 chars and be alphanumeric.';
    }

    var query = User.find({
      name: req.params.username
    });

    query.exec().then(function (users) {
      if (users.length === 0) {
        var user = new User({
          name: req.params.username,
          password: req.params.password
        });
        user.save(function (err) {
          if (err) {
            throw 'Error creating user: ' + err;
          } else {
            res.json({result: 'success'});
          }
        });
      } else {
        throw 'User already exists';
      }
    });

  } catch (e) {
    res.json({result: 'error', error: e});
  }
};

/**
 * Logins a user.
 *
 * @param req The request object. Must contain username and password params.
 * @param res The response object.
 */
module.exports.login = function(req, res) {
  var username = req.params.username;
  var password = req.params.password;
  User.findOne({name: username}, function (err, user) {
    if (err) {
      return res.send(500, {
        logged: false,
        message: err
      });
    }

    if (!user) {
      return res.send(401, {
        logged: false,
        message: 'Incorrect username'
      });
    }

    var payload = {
      username: user.name,
      /*jshint camelcase: false */
      userId: user._id
      /*jshint camelcase: true */
    };

    if (user.password !== password) {
      return res.send(401, {
        logged: false,
        message: 'Incorrect password'
      });
    }

    return res.json({
      logged: true,
      token: jwt.sign(payload, secret, options)
    });
  });
};

/**
 * Checks user login status.
 *
 * @param req The request object. Must contain username and password params.
 * @param res The response object.
 */
module.exports.session = function (req, res) {
  getLoggedUser(req).then(function (user) {
    res.json({
      loggedUserName: user.name
    });
  }, function () {
    res.json({
      loggedUserName: ''
    });
  });
};

/**
 * Logouts the user.
 *
 * @param req The request object. It must contain the authorization header.
 * @param res The response object.
 */
module.exports.logout = function (req, res) {
  var token = req.headers.authorization;
  jwt.verify(token, secret, function (err, decoded) {
    sockets.removeAll(decoded.userId);
    res.json({
      loggedUserName: ''
    });
  });
};

/**
 * Exposes the getLoggedUser function to other modules.
 *
 * @type {getLoggedUser}
 */
module.exports.getLoggedUser = getLoggedUser;
