'use strict';

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var secret = require('../config/index').token.secret;
var User = require('../models/index').userModel;
var logger = require('../services/logger');

var sockets = {};

/**
 * Authenticate socket using token.
 *
 * @param socket The socket to authenticate.
 * @param data A data object containing the token.
 */
var authenticate = function(socket, data) {
  jwt.verify(data.token, secret, function(err, decoded) {
    if (err) {
      return false;
    }
    /* jshint camelcase:false */
    socket.decoded_token = decoded;
    /* jshint camelcase:true */
    return true;
  });
};

/**
 * Initializes a new socket.
 *
 * @param socket The new socket.
 */
module.exports.init = function (socket) {
  /* jshint camelcase:false */
  if (!socket.handshake.decoded_token) {
    authenticate(socket, socket.handshake.query);
  }

  var userId = socket.decoded_token ? socket.decoded_token.userId : 'default';
  /* jshint camelcase:true */
  var socketId = socket.id;

  if (!sockets[userId]) {
    sockets[userId] = {};
  }

  sockets[userId][socketId] = socket;
  logger.info('socket for user: ' + userId + ' with id: ' + socketId);

  socket.on('disconnect', function () {
    if (sockets[userId][socketId]) {
      delete sockets[userId][socketId];
      logger.info('user: ' + userId + ' socket: ' + socketId + ' disconnected');
    }
  });
};

/**
 * Notifies sockets of a news item. Only notifies the users having the news' feed.
 *
 * @param news The news object.
 */
module.exports.notifySockets = function (news) {
  _.forOwn(sockets, function (userSockets, userId) {
    User.findById(userId, function (err, user) {
      if (user) {
        var userFeedIds = _.pluck(user.feeds, 'id');
        var newsFeedIds = _.pluck(news.feeds, 'id');
        var commonFeeds = _.intersection(userFeedIds, newsFeedIds);
        if (commonFeeds.length > 0) {
          _.forEach(userSockets, function (socket) {
            socket.emit('news', news);
          });
        }
      }
    });
  });
};

/**
 * Removes all sockets for a user.
 *
 * @param userId
 */
module.exports.removeAll = function (userId) {
  if (sockets[userId]) {
    delete sockets[userId];
  }
};
