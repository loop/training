'use strict';

var twitterStream = require('./twitter-stream');
var config = require('../config');
var jQuery = require('jquery-deferred');
var twitter = new require('ntwitter')(config.twitterApiCredentials);
var Feed = require('../models').feedModel;
var logger = require('../services/logger');
var _ = require('lodash');

var obtainRemoteId = function () {};
var obtainTweets = function () {};

var createTwitterFeed = function (uri, type, user) {
  var responseDeferred = jQuery.Deferred();

  var query = Feed.findOne({
    uri: uri,
    type: type
  });
  query.exec(function (err, existingFeed) {
    if (!err && !existingFeed) {
      obtainRemoteId().then(function (remoteId) {
        var feed = new Feed({
          uri: uri,
          remoteId: remoteId,
          type: type
        });

        feed.save(function (err) {
          if (err) {
            logger.error('Error creating feed: ' + err);
            responseDeferred.reject({result: 'error', error: 'Error creating feed: ' + err});
          } else {
            user.feeds.push(feed);
            user.save();
            obtainTweets(remoteId).then(function (data) {
              _.forEach(data, function (tweetData) {
                twitterStream.saveNews(tweetData, [feed._id]);
              });
              // Restart twitter stream
              twitterStream.restart();
              responseDeferred.resolve({result: 'success'});
            });
          }
        });
      }, function (err) {
        logger.error('Error creating feed: ' + err);
        responseDeferred.reject({result: 'error', error: 'The twitter user does not exists'});
      });
    } else {
      /*jshint camelcase: false */
      if (!_.find(user.feeds, {id: existingFeed._id.id})) {
        /*jshint camelcase: true */
        user.feeds.push(existingFeed);
        user.save();
        responseDeferred.resolve({result: 'success'});
      } else {
        responseDeferred.reject({result: 'error', error: 'Feed already added'});
      }
    }
  });

  return responseDeferred.promise();
};

module.exports.createTwitterUserFeed = function (uri, type, user) {
  obtainRemoteId = function () {
    var deferred = jQuery.Deferred();

    twitter.showUser(uri, function (err, data) {
      if (!err && data[0]) {
        /*jshint camelcase: false */
        var twitterId = data[0].id_str;
        /*jshint camelcase: true */
        deferred.resolve(twitterId);
      } else {
        deferred.reject();
      }
    });

    return deferred.promise();
  };

  obtainTweets = function (twitterId) {
    var deferred = jQuery.Deferred();
    /*jshint camelcase: false */
    var userConfig = {user_id: twitterId, count: 10};
    /*jshint camelcase: true */
    twitter.getUserTimeline(userConfig, function (err, data) {
      if (!err && data.length > 0) {
        deferred.resolve(data);
      }
    });

    return deferred.promise();
  };

  return createTwitterFeed(uri, type, user);
};

module.exports.createTwitterHashtagFeed = function (uri, type, user) {
  obtainRemoteId = function () {
    var deferred = jQuery.Deferred();
    deferred.resolve(uri);

    return deferred.promise();
  };

  obtainTweets = function (hashtag) {
    var deferred = jQuery.Deferred();
    var hashtagConfig = {q: '#' + hashtag, count: 10};
    twitter.get('/search/tweets.json', hashtagConfig, function (err, data) {
      if (!err && data.statuses && data.statuses.length > 0) {
        deferred.resolve(data);
      }
    });

    return deferred.promise();
  };

  return createTwitterFeed(uri, type, user);
};
