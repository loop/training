/**
 * Manages all feed related operations.
 */

'use strict';

var Feed = require('../models').feedModel;
var authentication = require('../services/authentication');
var twitterService = require('../services/twitter');

module.exports.createFeed = function (req, res) {
  authentication.getLoggedUser(req).then(function (user) {
    if (user) {
      var uri = req.params.uri;
      var type = req.params.type;

      if (type === 'twitter') {
        twitterService.createTwitterUserFeed(uri, type, user).then(function (response) {
          res.json(response);
        }, function (response) {
          res.json(response);
        });
      } else if (type === 'twitterHashtag') {
        twitterService.createTwitterHashtagFeed(uri, type, user).then(function (response) {
          res.json(response);
        }, function (response) {
          res.json(response);
        });
      }
    }
  });
};

module.exports.getFeeds = function (req, res) {
  authentication.getLoggedUser(req).then(function (user) {
    Feed.find({'_id': {$in: user.feeds}}, function (err, feeds) {
      res.json(feeds);
    });
  });
};

module.exports.removeFeed = function (req, res) {
  authentication.getLoggedUser(req).then(function (user) {
    var feedId = req.params.feedId;
    user.feeds.remove(feedId);
    user.save();
    Feed.find({'_id': {$in: user.feeds}}, function (err, feeds) {
      res.json({
        result: 'success',
        feeds: feeds
      });
    });
  });
};
