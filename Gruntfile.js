'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);
  var config = require('./server/config');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    'node-inspector': {
      custom: {
        options: {
          'web-host': 'localhost',
          'web-port' : 8088
        }
      }
    },

    nodemon: {
      debug: {
        script: 'server/app.js',
        options: {
          nodeArgs: ['--debug'],
          env: {
            PORT: config.port
          },
          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            // files browser on initial server start
            nodemon.on('config:update', function () {
            });
          }
        }
      }
    },

    open: {
      server: {
        url: 'http://localhost:9000/'
      }
    },

    compass: {
      options: {
        sassDir: 'client/styles',
        cssDir: 'client/styles',
        javascriptsDir: 'client/scripts',
        importPath: 'client/bower_components',
        relativeAssets: false,
        assetCacheBuster: false,
        raw: 'Sass::Script::Number.precision = 10\n'
      },
      server: {
        options: {
          debugInfo: true
        }
      }
    },

    watch: {
      js: {
        files: ['client/scripts/{,*/}*.js', 'server/{,*/}*.js'],
        tasks: ['newer:jshint:all']
      },
      jsTest: {
        files: ['client/scripts/{,*/}*.js', 'client/test/*.js'],
        tasks: ['karma']
      },
      compass: {
        files: ['client/styles/{,*/}*.{scss,sass}'],
        tasks: ['compass:server']
      },
      gruntfile: {
        files: ['Gruntfile.js'],
        tasks: ['newer:jshint:all']
      },
      livereload: {
        files: [
          'client/*.html'
        ]
      },
      options: {
        livereload: true
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          'client/scripts/{,*/}*.js',
          'server/{,*/}*.js',
        ]
      }
    },

    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true,
        reporters: 'progress',
        runnerPort: 9998
      }
    },
    /* jshint ignore:start */
    auto_install: {
      local: {}
    },
    /* jshint ignore:end */

    concurrent: {
      debug: {
        tasks: [
          'watch',
          'open',
          'nodemon',
          'node-inspector'
        ],
        options: {
          logConcurrentOutput: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-node-inspector');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-auto-install');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('test', ['karma']);

  grunt.registerTask('setup', ['auto_install']);

  grunt.registerTask('serve', function () {
    return grunt.task.run([
      'jshint',
      'test',
      'compass',
      'concurrent:debug'
    ]);
  });

  grunt.registerTask('default', ['serve']);
};
