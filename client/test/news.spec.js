define(['angular', 'angular-mocks', 'angular-cookies', 'app', 'infinite-scroll', 'ngIdle'], function() {
  'use strict';

  describe('Controller: NewsController', function () {

    // load the controller's module
    beforeEach(module('ngCookies', 'trainingApp.controllers.NewsController','trainingApp.services.SocketService',
        'trainingApp.constants.Constants', 'infinite-scroll'));

    var NewsController,
        $httpBackend,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, _$cookieStore_, _SocketService_) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/api/news')
        .respond([{title: 'news', body: 'news body'},{title: 'news 2', body: 'news body 2'}]);

      $httpBackend.when('GET', '/api/feeds')
          .respond([
          {_id: '001', uri: 'uri001', type: 'twitter'},
          {_id: '002', uri: 'uri002', type: 'twitterHashtag'}
        ]);

      scope = $rootScope.$new();
      NewsController = $controller('NewsController', {
        $scope: scope,
        $cookieStore: _$cookieStore_,
        SocketService: _SocketService_
      });
    }));

    it('should attach a list of news to the scope', function () {
      $httpBackend.expectGET('/api/news');
      $httpBackend.expectGET('/api/feeds');
      $httpBackend.flush();
      expect(scope.news.length).to.equal(2);
      expect(scope.userFeeds.length).to.equal(2);
    });
  });
});
