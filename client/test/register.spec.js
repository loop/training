define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Controller: RegisterController', function () {

    // load the controller's module
    beforeEach(module('trainingApp.controllers.RegisterController'));

    var RegisterController,
        $httpBackend,
        $location,
        $timeoutInstance,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_$httpBackend_, $timeout, $controller, $rootScope, _$location_) {
      $httpBackend = _$httpBackend_;
      $timeoutInstance = $timeout;

      scope = $rootScope.$new();
      RegisterController = $controller('RegisterController', {
        $scope: scope
      });

      $location = _$location_;
    }));

    it('should register a new user', function () {
      var user = 'testuser@angular.org',
          pass = 'pass';

      $httpBackend.expectPOST('/api/register', {
        username: user,
        password: pass
      }).respond({result:'success'});

      scope.username  = user;
      scope.password  = pass;
      scope.password2 = pass;
      scope.register();

      $httpBackend.flush();
      $timeoutInstance.flush();
      expect($location.path()).to.equal('/login');
    });

    it('should validate username', function () {
      scope.username = 'baduser'
      scope.$digest();
      scope.register();

      expect(scope.msgError.username).to.equal('The username must be a valid email address.');
    });

    it('should validate password', function () {
      scope.username = 'testuser@angular.org';
      scope.$digest();
      scope.register();

      expect(scope.msgError.password).to.equal('The password must match, have at least 6 chars and be alphanumeric.');
    });

    it('should validate passwords match', function () {
      scope.username = 'user';
      scope.password = 'pass';
      scope.password2 = 'password';
      scope.$digest();
      scope.register();

      expect(scope.msgError.password).to.equal('The password must match, have at least 6 chars and be alphanumeric.');
    });
  });
});
