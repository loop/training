define(['angular', 'angular-mocks', 'angular-cookies', 'app'], function() {
  'use strict';

  describe('Controller: ManageController', function () {

    // load the controller's module
    beforeEach(module('ngCookies', 'trainingApp.controllers.ManageController'));

    var ManageController,
      $httpBackend,
      scope,
      feeds = [
        {uri: 'test1', type: 'twitter', remoteId: 'test1', _id: '1'},
        {uri: 'test2', type: 'twitter', remoteId: 'test2', _id: '2'},
        {uri: 'test3', type: 'twitter', remoteId: 'test3', _id: '3'},
        {uri: 'test4', type: 'twitter', remoteId: 'test4', _id: '4'}
      ];

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_$httpBackend_, $controller, $rootScope) {
      $httpBackend = _$httpBackend_;

      scope = $rootScope.$new();
      ManageController = $controller('ManageController', {
        $scope: scope
      });
    }));

    it('should serve user feeds', function () {
      $httpBackend.expectGET('/api/feeds').respond(feeds);
      $httpBackend.flush();

      expect(scope.feeds.length).to.equal(4);
    });

    it('should delete feed', function () {
      var feedId = 1;
      $httpBackend.expectGET('/api/feeds').respond(feeds);
      feeds.splice(0, 1);
      $httpBackend.expectDELETE('/api/feed/' + feedId).respond({result: 'success', feeds: feeds});

      scope.remove(feedId);
      $httpBackend.flush();

      expect(scope.feeds.length).to.equal(3);
    });
  });
});
