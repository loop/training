var allTestFiles = [];
var TEST_REGEXP = /test\/.*(spec|test)\.js$/i;

var pathToModule = function(path) {
  return path.replace(/^\/base\/client\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    allTestFiles.push(pathToModule(file));
  }
});

require.config({
  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base/client/scripts',

  paths: {
    angular: '../bower_components/angular/angular',
    'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
    'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
    'infinite-scroll': '../bower_components/ngInfiniteScroll/build/ng-infinite-scroll',
    'ngIdle': '../bower_components/ng-idle/angular-idle',
    'jquery': '../mocks/jquery',
    'socketio': '../mocks/socket',
    'masonry': '../mocks/masonry',
    'validator-js': '../bower_components/validator-js/validator',
    'test': '../test'
  },

  shim: {
    angular: {
      exports: 'angular'
    },
    'angular-cookies': [
      'angular'
    ],
    'infinite-scroll': [
      'angular'
    ],
    'ngIdle': [
      'angular'
    ],
    'angular-mocks': {
      deps: [
        'angular'
      ],
      exports: 'angular.mock'
    }
  },

  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff mocha, as it is asynchronous
  callback: window.__karma__.start
});
