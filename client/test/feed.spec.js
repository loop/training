define(['angular', 'angular-mocks', 'angular-cookies', 'app'], function(/*angular, mocks, app*/) {
  'use strict';

  describe('Controller: FeedController', function () {

    // load the controller's module
    beforeEach(module('ngCookies', 'trainingApp.controllers.FeedController'));

    var FeedController,
      $httpBackend,
      $location,
      scope,
      $timeoutInstance;

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, $timeout, _$location_) {
      $httpBackend = _$httpBackend_;

      scope = $rootScope.$new();
      FeedController = $controller('FeedController', {
        $scope: scope
      });
      $timeoutInstance = $timeout;
      $location = _$location_;
    }));

    it('should add a new feed', function () {
      var expects = {uri:'mocked', type: 'twitter'};
      $httpBackend.expectPOST('/api/feed', expects).respond({result:'success'});

      scope.uri = 'mocked';
      scope.type = 'twitter';
      scope.addFeed();

      $httpBackend.flush();
      $timeoutInstance.flush();
      expect($location.path()).to.equal('/#/');
    });

    it('should validate URI', function () {
      scope.type = 'twitter';
      scope.addFeed();

      expect(scope.error).to.equal('URI has errors!');
    });
  });
});
