define(['angular', 'angular-mocks', 'angular-cookies', 'app'], function() {
  'use strict';

  describe('Controller: LoginController', function () {

    // load the controller's module
    beforeEach(module('ngCookies', 'trainingApp.controllers.LoginController', 'trainingApp.services.AuthService',
        'trainingApp.constants.Constants'));

    var LoginController,
      $location,
      $timeoutInstance,
      $httpBackend,
      scope,
      AuthService;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope, $timeout, _$httpBackend_, _$location_, _$cookieStore_,
          _AuthService_) {
      scope = $rootScope.$new();
      LoginController = $controller('LoginController', {
        $scope: scope,
        $cookieStore: _$cookieStore_,
        AuthService: _AuthService_
      });
      $timeoutInstance = $timeout;
      $location = _$location_;
      $httpBackend = _$httpBackend_;
      AuthService = _AuthService_;
    }));

    it('should login a new user', function () {
      $httpBackend.expectPOST('/api/login',{username:'user',password:'pass'}).respond({result:'success'});
      var credentials = {
        'username': 'user',
        'password': 'pass'
      };
      scope.login(credentials);

      $httpBackend.flush();
      $timeoutInstance.flush();

      expect($location.path()).to.equal('/');
    });

    it('should validate username', function () {
      var credentials = {
        'username': '',
        'password': ''
      };
      scope.login(credentials);

      expect(scope.error).to.equal('Username is empty!');
    });

    it('should validate password', function () {
      var credentials = {
        'username': 'user',
        'password': ''
      };
      scope.login(credentials);

      expect(scope.error).to.equal('Password is empty!');
    });
  });
});
