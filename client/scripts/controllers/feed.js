define(['angular', 'jquery'], function (angular, $) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.FeedController
   * @description
   * # FeedController
   * Controller of the trainingApp
   */
  var module = angular.module('trainingApp.controllers.FeedController', []);
  module.controller('FeedController', function ($scope, $http, $location, $timeout, $cookieStore) {
    $scope.uri = '';
    $scope.type = 'twitter';
    $scope.spinnerShow = false;
    $scope.addFeed = function () {
      $scope.spinnerShow = true;
      var uri = $scope.uri;
      var type = $scope.type;
      var validates = {
        twitter : /^[@＠]?([a-zA-Z0-9_]{1,15})$/,
        twitterHashtag : /^[#]?([a-zA-Z0-9_]{1,139})$/
      };
      if (validates[type].test(uri)) {
        var token = $cookieStore.get('token');
        var config = {
          headers: {
            Authorization: token
          }
        };

        $http.post('/api/feed', {uri: uri, type: type}, config).success(function (response) {
          $scope.spinnerShow = false;
          if (response.result === 'success') {
            var messageSelector = $('#successModal');
            messageSelector.modal({
              keyboard: false,
              backdrop: 'static'
            });
            $timeout(
              function() {
                messageSelector.modal('hide');
                $location.path('#/');
              },1000
            );
          } else {
            $scope.error = response.error;
          }
        });
      } else {
        $scope.error = 'URI has errors!';
        $scope.spinnerShow = false;
      }
    };
  });
});
