define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.NewsController
   * @description
   * # NewsController
   * Controller of the trainingApp
   */
  var module = angular.module(
    'trainingApp.controllers.NewsController',
    ['infinite-scroll', 'ngIdle']
  );

  //News panel config
  var newsConfig = {
    fetchLoadLimit : 20,
    recentFlagDelay : 15000,
    idleDuration : 60
  };

  module.controller('NewsController', function ($scope, $http, $cookieStore, SocketService, $timeout, $idle) {
    var token = $cookieStore.get('token');

    var config = {
      headers: {
        Authorization: token
      }
    };
    var start = newsConfig.fetchLoadLimit;
    $idle.watch();
    $scope.news = [];
    $scope.newNews = [];
    $http.get('/api/news', config).success(function (news) {
      $scope.news = $scope.news.concat(news);
    });

    //By default show all the feeds
    $scope.feedToFilter = null;
    $scope.filterNews = function(){
      $scope.newNews = [];
      var configFilter = config;
      configFilter.params = {feedId: $scope.feedToFilter._id};
      $http.get('/api/news', configFilter).success(function (news) {
        $scope.news = news;
      });
    };

    $http.get('/api/feeds', config).success(function (feeds) {
      $scope.userFeeds = feeds;
    });

    var promiseRemoveRecent;
    var scheduleRemoveRecentMarks = function () {
      $timeout.cancel(promiseRemoveRecent);
      promiseRemoveRecent = $timeout(
        function () {
          var removeRecentMarks = function(value, key, obj) {
            obj[key].recent = false;
          };
          angular.forEach($scope.news, removeRecentMarks);
        }, newsConfig.recentFlagDelay);
    };

    var userIsActive = true;
    $scope.$on('$idleStart', function() {
      userIsActive = false;
    });

    SocketService.on('news', function (news) {
      if (!$scope.feedToFilter || news.feeds.indexOf($scope.feedToFilter._id) !== -1) {
        news.recent = true;
        if (window.scrollY === 0 && userIsActive) {
          if ($scope.newNews.length > 0) {
            $scope.newNews.unshift(news);
            $scope.showNewNews();
          } else {
            scheduleRemoveRecentMarks();
            $scope.news.unshift(news);
          }
        } else {
          $scope.newNews.unshift(news);
        }
      }
    });

    $scope.loadMore = function() {
      var configNext = config;
      configNext.params = {'start': start};

      //keep next fetch also filtered
      if ($scope.feedToFilter) {
        configNext.params.feedId = $scope.feedToFilter._id;
      }

      $http.get('/api/news', configNext).success(function (news) {
        $scope.news = $scope.news.concat(news);
        start += newsConfig.fetchLoadLimit;
      });
    };

    $scope.showNewNews = function() {
      userIsActive = true;
      scrollTo(0, 0);
      var news = $scope.newNews.concat($scope.news);
      $scope.news = [];
      $scope.$apply();
      $scope.news = news;
      $scope.newNews = [];
      scheduleRemoveRecentMarks();
    };
  }).config(function($idleProvider) {
    $idleProvider.idleDuration(newsConfig.idleDuration);
  });
});
