define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.HeaderController
   * @description
   * # HeaderController
   * Controller of the trainingApp
   */
  var module = angular.module('trainingApp.controllers.HeaderController', []);
  module.controller('HeaderController', function ($scope, $rootScope, AuthService, AUTH_EVENTS) {
    $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
      $scope.loggedIn = AuthService.isLoggedIn();
      $scope.username = AuthService.userLoggedIn();
    });

    $scope.logout = function () {
      AuthService.logout().then(function () {
        $scope.loggedIn = AuthService.isLoggedIn();
        $scope.username = AuthService.userLoggedIn();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
      });
    };
  });
});
