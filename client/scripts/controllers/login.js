define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.LoginController
   * @description
   * # LoginController
   * Controller of the trainingApp
   */
  var module = angular.module('trainingApp.controllers.LoginController', []);
  module.controller('LoginController', function ($scope, $rootScope, $http, $location, $timeout, AuthService,
      AUTH_EVENTS) {
    $scope.credentials = {
      username: '',
      password: ''
    };
    $scope.logging = false;

    $scope.login = function (credentials) {
      $scope.error = '';
      var username = credentials.username;
      var password = credentials.password;
      if (username && password && !$scope.logging) {
        $scope.logging = true;
        AuthService.login(username, password).then(function () {
          $scope.message = 'Login successful. Redirecting...';
          $timeout(function () {
            $scope.logging = false;
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            $location.path('');
          }, 1000);
        }, function (error) {
          var message = error.data ? error.data.message : error;
          $scope.logging = false;
          $scope.error = message;
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
      } else if (!username) {
        $scope.error = 'Username is empty!';
      } else if (!password) {
        $scope.error = 'Password is empty!';
      }
    };
  });
});
