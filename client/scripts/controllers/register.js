define(['angular', 'validator-js'], function (angular, validator) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.RegisterController
   * @description
   * # RegisterController
   * Controller of the trainingApp
   */
  var module = angular.module('trainingApp.controllers.RegisterController', []);

  module.controller('RegisterController', function ($scope, $http, $location, $timeout) {
    $scope.msgError = {};

    $scope.$watch('username', function() {
      if(validator.isEmail($scope.username)) {
        delete $scope.msgError.username;
      } else {
        $scope.emptyUsername = !$scope.username;
        $scope.msgError.username = 'The username must be a valid email address.';
      }
    });

    $scope.$watch('password + password2', function () {
      $scope.passwordsExist = !!$scope.password && !!$scope.password2;
      $scope.passwordsMatch =   $scope.password ===  $scope.password2;

      if($scope.passwordsMatch &&
         validator.isLength($scope.password, 6) &&
         validator.isAlphanumeric($scope.password)) {
        delete $scope.msgError.password;
      } else {
        $scope.msgError.password = 'The password must match, have at least 6 chars and be alphanumeric.';
      }
    });

    $scope.registering = false;
    $scope.register = function() {
      if(Object.keys($scope.msgError).length !== 0) {
        $scope.errors = [];
        angular.forEach($scope.msgError, function(v) {
            $scope.errors.push(v);
        });
      } else {
        $scope.errors = false;
        if(!$scope.msgError.username && !$scope.msgError.username && !$scope.registering) {
          $scope.registering = true;

          var data = {
            'username': $scope.username,
            'password': $scope.password
          };

          $http.post('/api/register', data).success(function (response) {
            if (response.result === 'success') {
                $scope.message = 'User registered successfully. Redirecting...';
                $timeout(function () {
                    $scope.registering = false;
                    $location.path('login');
                }, 1000);
            } else {
                $scope.registering = false;
                $scope.errors = [response.error];
            }
          });
        }
      }
    };
  });
});
