define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.controllers.ManageController
   * @description
   * # ManageController
   * Controller of the trainingApp
   */
  var module = angular.module('trainingApp.controllers.ManageController', []);
  module.controller('ManageController', function ($scope, $http, $cookieStore) {
    var token = $cookieStore.get('token');
    var config = {
      headers: {
        Authorization: token
      }
    };

    $http.get('/api/feeds', config).success(function (feeds) {
      $scope.feeds = feeds;
    });

    $scope.remove = function (feedId) {
      $http.delete('/api/feed/' + feedId, {headers: {Authorization: token}}).success(function (response) {
        $scope.message = 'Feed Deleted';
        $scope.feeds = response.feeds;
      });
    };
  });
});
