/*jshint unused: vars */
require.config({
  paths: {
    angular: '../bower_components/angular/angular',
    'angular-animate': '../bower_components/angular-animate/angular-animate',
    'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
    'angular-resource': '../bower_components/angular-resource/angular-resource',
    'angular-route': '../bower_components/angular-route/angular-route',
    'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize',
    'angular-scenario': '../bower_components/angular-scenario/angular-scenario',
    'angular-touch': '../bower_components/angular-touch/angular-touch',
    affix: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix',
    alert: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert',
    button: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button',
    carousel: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel',
    collapse: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse',
    dropdown: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown',
    tab: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab',
    transition: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition',
    scrollspy: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy',
    modal: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal',
    tooltip: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip',
    popover: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover',
    'es5-shim': '../bower_components/es5-shim/es5-shim',
    json3: '../bower_components/json3/lib/json3',
    requirejs: '../bower_components/requirejs/require',
    jquery: '../bower_components/jquery/dist/jquery',
    // TODO: remove server address from socket.io library location.
    socketio: 'http://localhost:9001/socket.io/socket.io',
    masonry: '../bower_components/masonry/masonry',
    'get-size': '../bower_components/get-size',
    'get-style-property': '../bower_components/get-style-property',
    item: '../bower_components/outlayer/item',
    outlayer: '../bower_components/outlayer',
    eventie: '../bower_components/eventie',
    eventEmitter: '../bower_components/eventEmitter',
    'jquery-bridget': '../bower_components/jquery-bridget/jquery.bridget',
    'angular-masonry': '../bower_components/angular-masonry/angular-masonry',
    'infinite-scroll': '../bower_components/ngInfiniteScroll/build/ng-infinite-scroll',
    'ngIdle': '../bower_components/ng-idle/angular-idle',
    imagesloaded: '../bower_components/imagesloaded/imagesloaded',
    'validator-js': '../bower_components/validator-js/validator'
  },
  shim: {
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    'angular-sanitize': [
      'angular'
    ],
    'angular-resource': [
      'angular'
    ],
    'angular-animate': [
      'angular'
    ],
    'angular-touch': [
      'angular'
    ],
    'infinite-scroll': [
      'angular'
    ],
    'ngIdle': [
      'angular'
    ],
    'modal':  [
      'jquery'
    ],
    'angular-masonry': {
      exports : 'angular-masonry',
      deps: [
        'jquery',
        'jquery-bridget',
        'angular',
        'masonry',
        'imagesloaded'
      ]
    }
  },
  packages: [
    {
      name: 'doc-ready',
      main: 'doc-ready.js',
      location: '../bower_components/doc-ready'
    },
    {
      name: 'matches-selector',
      main: 'matches-selector.js',
      location: '../bower_components/matches-selector'
    }
  ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
  'jquery',
  'angular',
  'app',
  'angular-route',
  'angular-cookies',
  'angular-sanitize',
  'angular-resource',
  'angular-animate',
  'modal',
  'angular-touch',
  'angular-masonry',
  'infinite-scroll',
  'ngIdle'
], function($, angular, app, ngRoutes, ngCookies, ngSanitize, ngResource, ngAnimate, ngTouch, modal) {
  'use strict';
  /* jshint ignore:start */
  var $html = angular.element(document.getElementsByTagName('html')[0]);
  /* jshint ignore:end */
  angular.element().ready(function() {
    angular.resumeBootstrap([app.name]);
  });
});
