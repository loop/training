define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.services.AuthService
   * @description
   * # AuthService
   * Service of the trainingApp
   */
  var module = angular.module('trainingApp.services.AuthService', []);
  module.factory('AuthService', function ($http, $cookieStore) {
    var loggedUsername = '';

    var authService = {};
    authService.login = function (username, password) {
      return $http.post('/api/login', {'username': username, 'password': password}).then(function (response) {
        if (response.data.logged) {
          $cookieStore.put('token', response.data.token);
          loggedUsername = username;
          return username;
        }
      });
    };

    authService.logout = function () {
      var token = $cookieStore.get('token');
      var config = {
        headers: {
          Authorization: token
        }
      };
      return $http.get('/api/logout', config).then(function(response) {
        loggedUsername = response.data.loggedUserName;
        $cookieStore.remove('token');
      });
    };

    authService.isLoggedIn = function () {
      return loggedUsername !== '';
    };

    authService.userLoggedIn = function () {
      return loggedUsername;
    };

    authService.currentUser = function () {
      var token = $cookieStore.get('token');
      var config = {
        headers: {
          Authorization: token
        }
      };
      return $http.get('/api/session', config).then(function(response) {
        if (response.data.loggedUserName) {
          loggedUsername = response.data.loggedUserName;
        }
      });
    };

    return authService;
  });
});
