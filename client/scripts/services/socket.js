define(['angular', 'socketio'], function (angular, io) {
  'use strict';

  /**
   * @ngdoc function
   * @name trainingApp.services.SocketService
   * @description
   * # SocketService
   * Service of the trainingApp
   */
  var module = angular.module('trainingApp.services.SocketService', []);
  module.factory('SocketService', function ($rootScope, $cookieStore, $timeout, AUTH_EVENTS) {
    var token = $cookieStore.get('token');
    // TODO: remove server address from connect call.
    var socket = io.connect('http://localhost:9001', {query: 'token=' + token, 'forceNew': true});
    var reconnect = function () {
      socket.disconnect(true);
      $timeout(function () {
        token = $cookieStore.get('token');
        socket = io.connect('http://localhost:9001', {query: 'token=' + token, 'forceNew': true});
      }, 5000);
    };
    $rootScope.$on(AUTH_EVENTS.loginSuccess, reconnect);
    $rootScope.$on(AUTH_EVENTS.logoutSuccess, reconnect);

    return {
      on: function (eventName, callback) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        });
      }
    };
  });
});
