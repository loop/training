/*jshint unused: vars */
define(['angular', 'controllers/news', 'controllers/register', 'controllers/login', 'controllers/feed',
    'controllers/header', 'controllers/manage', 'services/auth', 'services/socket', 'constants/index'],
    function (angular) {
  'use strict';

  /**
   * @ngdoc overview
   * @name trainingApp
   * @description
   * # trainingApp
   *
   * Main module of the application.
   */
  return angular
    .module('trainingApp', ['trainingApp.controllers.NewsController',
    'trainingApp.controllers.RegisterController',
    'trainingApp.controllers.LoginController',
    'trainingApp.controllers.FeedController',
    'trainingApp.controllers.ManageController',
    'trainingApp.controllers.HeaderController',
    'trainingApp.services.AuthService',
    'trainingApp.services.SocketService',
    'trainingApp.constants.Constants',
    /*angJSDeps*/
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngAnimate',
    'ngTouch',
    'wu.masonry'
  ]).config(function ($routeProvider, $keepaliveProvider, $idleProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'views/main.html',
          controller: 'NewsController'
        })
        .when('/register', {
          templateUrl: 'views/register.html',
          controller: 'RegisterController'
        })
        .when('/login', {
          templateUrl: 'views/login.html',
          controller: 'LoginController'
        })
        .when('/feed', {
          templateUrl: 'views/feed.html',
          controller: 'FeedController'
        })
        .when('/manage', {
          templateUrl: 'views/manage.html',
          controller: 'ManageController'
        })
        .otherwise({
          redirectTo: '/'
        });
    })
    .run(function ($rootScope, $location, AuthService, AUTH_EVENTS) {

      if (!AuthService.isLoggedIn()) {
        AuthService.currentUser().then(function () {
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        }, function () {
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
      }

      // On catching 401 errors, redirect to the login page.
      $rootScope.$on('event:auth-loginRequired', function() {
        $location.path('login');
        return false;
      });
    });
});
